$(document).ready(function() {

    // JSON Object Variables
    var jsonListingObject;
    var jsonUserObject;

    // Static Variables
    var currentListingID = 0;
    var currentUserID = 0;
    var iterator = 0;
    var numberOfListings = 0;
    var listingLatLng = {
        lat: 0,
        lng: 0
    };

    // URL Variables
    var listingURL = "http://apibeta.grabhouse.com/listing/property/pg/search?source=PG%20DC%20App&status=Uploaded";
    var listingEndpointURL = "http://apibeta.grabhouse.com/listing/property/pg/";
    var photoURL = "http://apibeta.grabhouse.com/upload/image/";
    var userURL = "http://apibeta.grabhouse.com/user/profile/";
    var flagURL = "assets/flags.json";

    // Function: Populate Data from JSON
    function populate(obj) {
        currentListingID = obj.listingId;

        $("#listing-id").val(obj.listingId);
        $("#listing-id-reject").html(obj.listingId);

        $("#neighborhood-photos div").empty();
        $("#kitchen-photos div").empty();
        $("#dining-area-photos div").empty();
        $("#common-room-photos div").empty();
        $("#washroom-photos div").empty();

        $("#amenities-form").trigger("reset");
        $("#services-form").trigger("reset");
        $("#food-form").trigger("reset");
        $("#rules-form").trigger("reset");



        $(obj.photos).each(function(index) {
            switch (obj.photos[index].title) {
                case "Neighborhood":
                    $('#neighborhood-photos div').append('<div class="photo"><input type="radio" data-title="Neighborhood" name="primaryPhoto" id="' + obj.photos[index].id + '" value="' + obj.photos[index].id + '"><a href="' + photoURL + obj.photos[index].id + '" target="blank"><img width="auto" height="100" src="' + photoURL + obj.photos[index].id + '" /></a></div>');
                    break;
                case "Dining Room":
                    $('#dining-area-photos div').append('<div class="photo"><input type="radio" data-title="Dining Room" name="primaryPhoto" value="' + obj.photos[index].id + '"><a href="' + photoURL + obj.photos[index].id + '" target="blank"><img width="auto" height="100" src="' + photoURL + obj.photos[index].id + '" /></a></div>');
                    break;
            }
        });

        $(obj.attributes.commonRoomMeta.photos).each(function(index) {
            $('#common-room-photos div').append('<div class="photo"><input type="radio" data-title="Common Room" name="primaryPhoto" id="' + obj.attributes.commonRoomMeta.photos[index].id + '" value="' + obj.attributes.commonRoomMeta.photos[index].id + '"><a href="' + photoURL + obj.attributes.commonRoomMeta.photos[index].id + '" target="blank"><img width="auto" height="100" src="' + photoURL + obj.attributes.commonRoomMeta.photos[index].id + '" /></a></div>');
        });

        $(obj.kitchen.photos).each(function(index){
            $('#kitchen-photos div').append('<div class="photo"><input type="radio" data-title="Common Room" name="primaryPhoto" id="' + obj.kitchen.photos[index].id + '" value="' + obj.kitchen.photos[index].id + '"><a href="' + photoURL + obj.kitchen.photos[index].id + '" target="blank"><img width="auto" height="100" src="' + photoURL + obj.kitchen.photos[index].id + '" /></a></div>');
        });

        $(obj.bathroom.photos).each(function(index){
            $('#washroom-photos div').append('<div class="photo"><input type="radio" data-title="Common Room" name="primaryPhoto" id="' + obj.bathroom.photos[index].id + '" value="' + obj.bathroom.photos[index].id + '"><a href="' + photoURL + obj.bathroom.photos[index].id + '" target="blank"><img width="auto" height="100" src="' + photoURL + obj.bathroom.photos[index].id + '" /></a></div>');
        });



        $("#pg-name").val(obj.pgName);



        // Get User Information from User JSON
        $(obj.people).each(function(index) {
            currentUserID = obj.people[index].userId;
            getUserJSON(currentUserID);

            var isOwner = $.inArray("isOwner", obj.people[index].entityRoles);
            if (isOwner !== -1) {
                $("#owner-name").val(jsonUserObject.data.firstName);
                $(jsonUserObject.data.mobiles).each(function(mobileIndex){
                    if(jsonUserObject.data.mobiles[mobileIndex].isPrimary)
                        $("#owner-primary-mobile-number").val(jsonUserObject.data.mobiles[mobileIndex].mobile);
                    else
                        $("#owner-secondary-mobile-number").val(jsonUserObject.data.mobiles[mobileIndex].mobile);
                });
            }

            var isManager = $.inArray("isManager", obj.people[index].entityRoles);
            if (isManager !== -1) {
                $("#manager-name").val(jsonUserObject.data.firstName);
                $(jsonUserObject.data.mobiles).each(function(mobileIndex){
                    if(jsonUserObject.data.mobiles[mobileIndex].isPrimary)
                        $("#manager-primary-mobile-number").val(jsonUserObject.data.mobiles[mobileIndex].mobile);
                    else
                        $("#manager-secondary-mobile-number").val(jsonUserObject.data.mobiles[mobileIndex].mobile);
                });

            }
            console.log("IsOwner " + isOwner + " IsManager " + isManager + " Data " + obj.people[index]);
        });

        if(obj.people.length > 1) {
            $("#manager-primary-mobile-number").prop('disabled', false);
            $("#manager-secondary-mobile-number").prop('disabled', false);
            $("#manager-name").prop('disabled', false);
        } else {
            $("#manager-primary-mobile-number").prop('disabled', true);
            $("#manager-secondary-mobile-number").prop('disabled', true);
            $("#manager-name").prop('disabled', true);
        }

        $("#gender").val('');
        $(obj.pgGender).each(function(index) {
            $('#gender option[value=' + obj.pgGender[index] + ']').attr('selected', true);
        });

        $("#house-no").val(obj.buildingDetails.houseNo);
        $("#road-name").val(obj.buildingDetails.roadName);
        $("#sub-locality").val(obj.buildingDetails.subLocality);
        $("#locality").val(obj.buildingDetails.locality);
        $("#city").val(obj.buildingDetails.city);
        $("#pincode").val(obj.buildingDetails.pincode);

        $("#contact-number").val();
        $("#landmark").val(obj.buildingDetails.landMark);


        // Sharing info
        $('#sharing').empty();
        $(obj.sharingInfo).each(function(index) {
            $('#sharing').append(
                '<div class="form-group"><label class="col-md-12">Sharing type ' + (index + 1) + '</label><div>' +
                '<div class="col-md-2"><div class="form-group"><label for="sharing-type">Sharing type</label><input disabled type="text" class="form-control" id="sharing-type-"' + index + ' value="' + obj.sharingInfo[index].sharingType + " Sharing" + '"/></div></div>' +
                '<div class="col-md-2"><div class="form-group"><label for="rent">Rent</label><div class="input-group"><span class="input-group-addon" id="basic-addon-"' + index + '>Rs.</span><input type="text" class="form-control" id="rent-"' + index + ' value="' + obj.sharingInfo[index].rent + '"/></div></div></div>' +
                '<div class="col-md-2"><div class="form-group"><label for="total-rooms">Total Rooms</label><input type="text" class="form-control" id="total-rooms-' + index + '"></div></div>' +
                '<div class="col-md-2"><div class="form-group"><label for="total-beds">Total Beds</label><input type="text" class="form-control" id="total-beds-' + index + '" value="' + obj.sharingInfo[index].totalBeds + '"/></div></div>' +
                '<div class="col-md-2"><div class="form-group"><label for="security-deposit">Security Deposit</label><div class="input-group"><span class="input-group-addon" id="basic-addon1">Rs.</span><input type="text" class="form-control" id="security-deposit-' + index + '" value="' + obj.sharingInfo[index].deposit + '"/></div></div></div>' +
                '<div class="col-md-2"><br/><div class="checkbox"><input type="checkbox" id="attached-bathroom-' + index + '"><label for="attached-bathroom">Attached Bathroom</label></div></div>' +
                '</div>' +
                '<div class="col-md-12 form-group" id="photo-container-' + index + '"/>' +
                '</div><div class="clearfix padding"></div>'
            );

            if (obj.sharingInfo[index].attributes.attachedBathroom)
                $('#attached-bathroom-' + index).prop('checked', true);

            $(obj.sharingInfo[index].photos).each(function(photoIndex) {
                $('#photo-container-' + index).append('<div class="photo"><a href="' + photoURL + obj.photos[index].id + '" target="blank"><img width="auto" height="100" src="' + photoURL + obj.sharingInfo[index].photos[photoIndex].id + '" /></a></div>');
            });
        });


        $("#notice-period").val(String(obj.attributes.noticePeriodInDays));
        $("#maintenance").val(String(obj.sharingInfo[0].maintenance));


        if (obj.attributes.internetConnectionAvailable)
            $('#internet').prop('checked', true);
        $("#bandwidth").val(String(obj.attributes.internetMeta.monthlyInternetCost));

        $("#cleaning").prop('checked', true);
        $("#frequency-room").val(obj.attributes.cleaningServicesMeta.room);
        $("#frequency-toilets").val(obj.attributes.cleaningServicesMeta.toilets);


        $('#breakfast option[value=' + obj.attributes.breakfastTimings + ']').attr('selected', true);
        $('#lunch option[value=' + obj.attributes.lunchTimings + ']').attr('selected', true);
        $('#dinner option[value=' + obj.attributes.dinnerTimings + ']').attr('selected', true);


        $('#food-type').val('');
        $(obj.attributes.foodType).each(function(index) {
            $('#food-type option[value=' + obj.attributes.foodType[index] + ']').attr('selected', true);
        });

        if (obj.attributes.fridgeAvailable)
            $('#refrigerator').prop('checked', true);

        if (obj.attributes.washingMachineAvailable)
            $('#washing-machine').prop('checked', true);

        if (obj.attributes.elevatorLiftAvailable)
            $('#elevator').prop('checked', true);

        if (obj.attributes.hotWaterAvailable)
            $('#hot-water').prop('checked', true);


	if(obj.attributes.timeRestriction)
            $("#time-restriction").val(String(obj.attributes.timeRestrictionMeta.entryAllowedTill));
        $("#guests-allowed").prop('checked', obj.attributes.guestsAllowed);
        $("#other-rules").val(String(obj.attributes.anyOtherPgRestrictions[0]));

        listingLatLng.lat = parseFloat(obj.buildingDetails.coordinates.lat);
        listingLatLng.lng = parseFloat(obj.buildingDetails.coordinates.lng);
        initMap();

        $(obj.accommodationFor).each(function(index) {
            $('#accommodation-for option[value=' + obj.accommodationFor[index] + ']').attr('selected', true);
        });
    }

    function uploadUserData(obj) {
        // Get User Information from User JSON
        $(obj.people).each(function(index) {
            currentUserID = obj.people[index].userId;
            getUserJSON(currentUserID);

            var isManager = $.inArray("isManager", obj.people[index].entityRoles);
            if (isManager !== -1) {
                jsonUserObject.data.firstName = $("#manager-name").val();

                if(!checkValidMobile($("#manager-primary-mobile-number").val())) {
                    return false;
                }

                if(jsonUserObject.data.mobiles.length == 2) {
                    jsonUserObject.data.mobiles.pop();
                }
                jsonUserObject.data.mobiles.pop();
                jsonUserObject.data.mobiles.push({
                    "isPrimary" : true,
                    "mobile" : $("#manager-primary-mobile-number").val(),
                    "countryCode" : "91"
                });

                var managerSecondaryMobile = $('#manager-secondary-mobile-number').val();
                if(managerSecondaryMobile.length > 0) {
                    if(!checkValidMobile(managerSecondaryMobile)) {
                        return false;
                    }

                    jsonUserObject.data.mobiles.push({
                        "isPrimary" : false,
                        "mobile" : managerSecondaryMobile,
                        "countryCode" : "91"
                    });
                }
            }


            var isOwner = $.inArray("isOwner", obj.people[index].entityRoles);
            if (isOwner !== -1) {
                jsonUserObject.data.firstName = $("#owner-name").val();

                if(!checkValidMobile($("#owner-primary-mobile-number").val())) {
                    return false; 
                }

                if(jsonUserObject.data.mobiles.length == 2) {
                    jsonUserObject.data.mobiles.pop();
                }
                jsonUserObject.data.mobiles.pop();
                jsonUserObject.data.mobiles.push({
                    "isPrimary" : true,
                    "mobile" : $("#owner-primary-mobile-number").val(),
                    "countryCode" : "91"
                });

                var ownerSecondaryMobile = $('#owner-secondary-mobile-number').val();
                if(ownerSecondaryMobile.length > 0) {
                    if(!checkValidMobile(ownerSecondaryMobile)) {
                        return false;
                    }

                    jsonUserObject.data.mobiles.push({
                        "isPrimary" : false,
                        "mobile" : ownerSecondaryMobile,
                        "countryCode" : "91"
                    });
                }
            }
            console.log("IsOwner " + isOwner + " IsManager " + isManager + " Data " + JSON.stringify(jsonUserObject));
            postUserJSON(obj.people[index].userId);
        });
        return true;
    }


    // Function to Update Values in jsonObject
    function updateJSON(obj) {
        obj.updatedBy = 1453477572315637;
        obj.buildingDetails.updatedBy = 1453477572315637;
        obj.pgName = $("#pg-name").val();
   
        obj.pgGender = $("#gender").val();
        obj.buildingDetails.houseNo = $("#house-no").val();
        obj.buildingDetails.roadName = $("#road-name").val();
        obj.buildingDetails.subLocality = $("#sub-locality").val();
        obj.buildingDetails.locality = $("#locality").val();
        obj.buildingDetails.landMark = $("#landmark").val();
        obj.buildingDetails.city = $("#city").val();
        obj.buildingDetails.pincode = $("#pincode").val();


        $(obj.photos).each(function(index) {
            if($("#" + obj.photos[index].id).prop('checked'))
                obj.photos[index].isPrimary = true;
        });

        $(obj.attributes.commonRoomMeta.photos).each(function(index) {
            if($("#" + obj.attributes.commonRoomMeta.photos[index].id).prop('checked'))
                obj.attributes.commonRoomMeta.photos[index].isPrimary = true;
        });

        $(obj.kitchen.photos).each(function(index){
            if($("#" + obj.kitchen.photos[index].id).prop('checked'))
                obj.kitchen.photos[index].isPrimary = true;
        });

        $(obj.bathroom.photos).each(function(index){
            if($("#" + obj.bathroom.photos[index].id).prop('checked'))
                obj.bathroom.photos[index].isPrimary = true;
        });


        $(obj.sharingInfo).each(function(index){
            obj.sharingInfo[index].deposit = parseInt($("#security-deposit-"+index).val());
            obj.sharingInfo[index].rent = parseInt($("#rent-"+index).val());
            obj.sharingInfo[index].totalBeds = parseInt($("#total-beds-"+index).val());
            obj.sharingInfo[index].totalRooms = parseInt($("#total-rooms-"+index).val());
            obj.sharingInfo[index].attachedBathroom = $("#attached-bathroom-" + index).prop('checked');
            obj.sharingInfo[index].maintenance = parseInt($("#maintenance").val());
        });
        
        obj.attributes.noticePeriodInDays = parseInt($("#notice-period").val());

        obj.attributes.internetConnectionAvailable = $('#internet').prop('checked');
        obj.attributes.cleaningServicesMeta.room = $("#frequency-room").val();
        obj.attributes.cleaningServicesMeta.toilets = $("#frequency-toilets").val();

        obj.attributes.foodType = $('#food-type').val();

        obj.attributes.fridgeAvailable = $('#refrigerator').prop('checked');
        obj.attributes.washingMachineAvailable = $('#washing-machine').prop('checked');
        obj.attributes.elevatorLiftAvailable = $('#elevator').prop('checked');
        obj.attributes.hotWaterAvailable = $('#hot-water').prop('checked');

        //obj.attributes.timeRestrictionMeta.entryAllowedTill = $("#time-restriction").val();
        obj.attributes.guestsAllowed = $('#guests-allowed').prop('checked');
        obj.accommodationFor = $('#accommodation-for').val();

    }

    function checkValidMobile(mobile) {
        if(mobile.length > 0) {
            if(mobile.charAt(0) === "9" || mobile.charAt(0) === "8" || mobile.charAt(0) === "7") {
                if(mobile.length === 10) {
                    return true;
                } else {
                    window.alert("Mobile number should be 10 chars long!");
                    return false;
                }
            } else {
                window.alert("Enter valid mobile number!");
                return false;
            }
        }
        window.alert("Invalid Mobile number!");
        return false;
    }

    // Function to Get Listing JSON
    function getListingJSON() {
        $.ajax({
            async: false,
            type: 'GET',
            dataType: "json",
            url: listingURL,
            success: function(jsonRawData) {
                jsonListingObject = jsonRawData;
                numberOfListings = console.log(jsonListingObject.data.length);
                processListingArray();
            }
        });
    }

    // Function to Get User JSON
    function getUserJSON(userID) {
        $.ajax({
            async: false,
            type: 'GET',
            dataType: "json",
            url: userURL + userID,
            success: function(jsonRawData) {
                jsonUserObject = jsonRawData;
            }
        });
    }

    // Function to Get Flags JSON
    function getFlagJSON() {
        $.ajax({
            async: false,
            type: 'GET',
            dataType: "json",
            url: flagURL,
            success: function(jsonRawData) {
                jsonFlagObject = jsonRawData;
                processFlagsFromListingArray();
            }
        });
    }

    // Function to Populate Form Elements using jsonListingData
    function processListingArray() {
        if (jsonListingObject) {
            populate(jsonListingObject.data[iterator]);
        }
    }

    // Function to POST data to Listing Endpoint
    function postListingJSON() {
        // Send jsonListingData[iterator] to API
        console.log(JSON.stringify(jsonListingObject.data[iterator]));
        $.ajax({
            type: "PUT",
            url: listingEndpointURL + currentListingID,
            data: JSON.stringify(jsonListingObject.data[iterator]),
            success: function(data) {
                iterator++;
                populate(jsonListingObject.data[iterator]);
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                if (iterator === numberOfListings) {
                    getJSON();
                    console.log("new json receieved!");
                }
            },
            error: function(data) {
                console.log(data)
            },
            contentType: 'application/json'
        });
    }

    // Function to POST data to User Endpoint
    function postUserJSON(userID) {
        // Send jsonListingData[iterator] to API
        $.ajax({
            type: "PUT",
            url: userURL + userID,
            data: JSON.stringify(jsonUserObject.data),
            success: function(data) {
                console.log("User data uploaded!");
            },
            error: function(data) {
                console.log(data)
            },
            contentType: 'application/json'
        });
    }

    // Add Flags to Application if any exist
    function processFlagsFromListingArray() {
        if (jsonFlagObject) {
            if (jsonFlagObject.landmark.nearby) {
                var landmarkFlag = '<div class="alert alert-danger"><strong>Landmark:</strong> distance is far away from given landmark!</div>'
                $('#flags').append(landmarkFlag);
            }
            if (jsonFlagObject.rent["2"].outlier) {
                var rentFlag = '<div class="alert alert-danger"><strong>Rent:</strong> rent is more than what was specified!</div>'
                $('#flags').append(rentFlag);
            }
            if (jsonFlagObject.locality.inside) {
                var localityFlag = '<div class="alert alert-danger"><strong>Locality:</strong> listing is not in the locality specified!</div>'
                $('#flags').append(localityFlag);
            }
        }
    }

    // Function for Initializing Google Maps
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: listingLatLng
        });

        var marker = new google.maps.Marker({
            position: listingLatLng,
            map: map
        });
    }

    // Function: Parsing JSON Listing Data
    getListingJSON();

    // What should happen when accept button is clicked?
    $("#accept-button").click(function() {
        if(uploadUserData(jsonListingObject.data[iterator])) {
            updateJSON(jsonListingObject.data[iterator]);

            // Change Listing Status to Active
            jsonListingObject.data[iterator].status = "Accepted";
            console.log(JSON.stringify(jsonListingObject.data[iterator]));
            // postListingJSON();
            // postUserJSON(currentUserID);
        } else {
console.log("I should not be here!");
}
    });

    // What should happen when skip button is clicked?
    $("#skip-button").click(function() {
        iterator++;
        populate(jsonListingObject.data[iterator]);
        $("html, body").animate({
            scrollTop: 0
        }, "fast");
        if (iterator === numberOfListings) {
            getJSON();
            console.log("new json receieved!");
        }
    });

    // What should happen when reject button is clicked?
    $("#reject-button").click(function() {
        // Change Listing Status to Deactivated
        jsonListingObject.data[iterator].status = "Rejected";

        iterator++;
        populate(jsonListingObject.data[iterator]);
        $("html, body").animate({
            scrollTop: 0
        }, "fast");
        if (iterator === numberOfListings) {
            getJSON();
            console.log("new json receieved!");
        }
    });


    $('.photo img').mouseenter(function() {
        $('img.hover-image').remove();
        $(this).parent().append('<img class="hover-image" height="400" src="' + $(this).attr('src') + '"/>')
    }).mouseleave(function() {
        $('img.hover-image').remove();
    })

    // Function: Parsing JSON Flags Data
    getFlagJSON();

});